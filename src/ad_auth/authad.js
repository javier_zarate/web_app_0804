import { UserAgentApplication } from "msal"

export {getUserAd};
    
var msalConfig = {
    auth: {
        clientId: "bcecc9e2-111e-4fd3-9069-d9325cff02f5",
        authority: "https://login.microsoftonline.com/336e740d-6447-4977-ad46-cefd0170c9e6",
        redirectURI: "http://localhost:8081/"
    },
    cache: {
        cacheLocation: "localStorage",
        storeAuthStateInCookie: true
    }
};

var requestObjAd = {
    scopes: ["user.read"]
};

var myMsalObjAd = new UserAgentApplication(msalConfig);

var loginAd = async() =>{
    var authResultAd = await myMsalObjAd.loginPopup(requestObjAd);
    return authResultAd.account;
};

var getAccountAd = async() =>{
    var account = await myMsalObjAd.getAccount();
    return account;
}

var logoutAd = async() =>{
    myMsalObjAd.logout();
}

function getUserAd(){
    return sessionStorage.getItem('msal.idtoken');
    /*const token_created = sessionStorage.getItem('msal.idtoken');
    return token_created;
    */
}

export default {
    loginAd,
    getAccountAd,
    logoutAd,
};








