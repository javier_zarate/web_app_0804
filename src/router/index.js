import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase'
import {getUserAd} from "./../ad_auth/authad"

var user = null;

Vue.use(VueRouter)


const router = new VueRouter({
  mode: 'history',
  routes: [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "about" */ '../views/Home.vue')
  },
  {
    path: '/issue',
    name: 'Issue',
    component: () => import(/* webpackChunkName: "about" */ '../views/Issue.vue')
  },
  {
    path: '/report',
    name: 'Report',
    component: () => import(/* webpackChunkName: "about" */ '../views/Report.vue')
    
    /*
    ,meta: {
      requiresAuth: true
    }
    */
  }
  ]
});

    /*
router.beforeEach((to, from, next) =>{
  if(to.matched.some(record =>record.meta.requiresAuth)){
    const idtoken = getUserAd();
    alert("user logged");
    alert(idtoken)

    
    if(!idtoken){
      next({
        name: 'Home'
      })
    }
    else{
      next();
    }
    
  }
  else{
    next();
  }
})
*/


export default router;
